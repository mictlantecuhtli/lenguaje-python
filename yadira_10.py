#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-01-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Calcule la sumatoria de N numeros
suma_Numeros = []
sumatoria = 0

cantidad_Numeros = int(input("¿Cuantos numeros desea sumar?: "))

for i in range (0, cantidad_Numeros, 1):
    print("Digite el numero ",i + 1)
    val=int(input())
    suma_Numeros.append(val)
    sumatoria = sumatoria + suma_Numeros[i]

print("La sumatoria es de: ", sumatoria)
