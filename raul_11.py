#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 01-01-2021                  #
#Autor: Fatima Azucena MC           #
#fatimaazucenamartinez274@gmail.com #
#####################################

#Concatenar 3 numeros enteros y mostrar su
#resultado, ejemplo, a=1, b=2, c=3, d=123

edad = 0

anyos = int(input("Ingrese su edad en años: "))
meses = int(input("Con cuantos meses: "))

edad = edad + (anyos*12)+meses

print ("Su edad en meses es:",edad)


