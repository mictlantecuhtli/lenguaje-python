#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 24-11-2020                  #
#Autor: Fatima Azucena MC           #
#fatimaazucenamartinez274@gmail.com #
#####################################

#Escriba un algoritmo que, dado el número de horas trabajadas por un empleado 
#y el sueldo por hora, calcule el sueldo total de ese empleado.

nombre = (input("Ingrese el nombre del empelado: "))
hTrabajadas =input ("Ingrese las horas trabajadas: ")
precioHora = input("Ingrese el precio por hora: ")

sueldoTotal = hTrabajadas*precioHora

print "El sueldo de ", nombre ," es de: $", sueldoTotal ,"pesos" 
