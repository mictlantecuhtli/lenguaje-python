#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 26-09-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#26-El banco “Bandido de peluche” desea calcular para cada uno de sus
#N clientes su saldo actual, su pago mínimo y su pago para no generar intereses.
#Además, quiere calcular el monto de lo que ganó por con­cepto interés con los 
#clientes morosos. Los datos que se conocen de cada cliente son: saldo anterior, 
#monto de las compras que realizó y pago que depositó en el corte anterior. 
#Para calcular el pago mínimo se considera 15% del saldo actual, y el pago 
#para no generar intere­ses corresponde a 85% del saldo actual, considerando 
#que el saldo actual debe incluir 12% de los intereses causados por no realizar
#el pago mínimo y $200 de multa por el mismo motivo. Realice el algo­ritmo correspondiente
#y represéntelo mediante diagrama de flujo y pseudocódigo.

numClientes = int(input("Inserte el numero de trabajadores: "))

for i in range (0,numClientes,1):
    nombre = raw_input("Inserte el nombre del trabajador: ")
    saldoAnterior = input("Inserte el saldo anterior: ")
    depositoAnterior = input("Ingrese su ultimo deposito: ")
    montoCompras = input("Inserte el monto por sus compras realizadas: ")
    saldoActual = input("Inserte el saldo actual: ")
    pagoActual = (saldoActual*0.12)+200
    pagoMinimo = (saldoActual*0.15)
    pagoInteres = (saldoActual*0.85)
    print "El saldo actual de ",nombre," es de: $",pagoActual," pesos"
    print "El saldo minimo de ",nombre," es de: $",pagoMinimo," pesos"
    print "El saldo para no generar intereses de ",nombre," es de: $",pagoInteres," pesos"
