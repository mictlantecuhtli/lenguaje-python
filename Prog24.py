#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 25-08-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################
 
#La CONAGUA requiere determinar el pago que debe realizar una persona por el total de metros cúbicos que consume de agua.
precioMetro = input ("Ingrese el precio por metro cubico: ");
metro = input ("Ingrese los metros cubicos de agua consumida: ");

resultado=(precioMetro*metro);

print "El total a pagar por los ", metro , "es de: $", resultado ," pesos"
