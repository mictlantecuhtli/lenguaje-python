#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

DESCUENTO = 0.35
precio = 0
cadena = 0

cadena = (input("Ingrese el nombre del medicamento: "))
precio = (input("Ingrese el precio del medicamento: "))

precio = precio-(precio*DESCUENTO)

print ("El total a pagar por su ", cadena ," es de: $", precio, " pesos")

