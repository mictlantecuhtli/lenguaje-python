#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 17-11-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################
  
#Escriba un algoritmo que dada la cantidad de monedas de 5-10-20 pesos, 
#diga la cantidad de dinero que se tiene en total.

resultado=0
billetes=[]
val=0
cajero = [1000, 500, 200, 100, 50, 20,10, 5, 2, 1, 0.50, 0.20, 0.10]

for i in range (len(cajero)) :
    print "Ingrese cuanto dinero de ",cajero[i]," tiene: "
    val=(input())
    billetes.append(val)
    resultado = resultado + (billetes[i] * cajero[i])

print "El total almacenado es de: $",resultado," pesos"
