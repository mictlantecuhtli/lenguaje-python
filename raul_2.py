#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Calcula el precio de un boleto de viaje, tomando en cuenta 
#el número de kilómetros que se van a recorrer. El precio 
#por Kilometro es de $20.50

PRECIOKILOMETRO=20.50

kilometrosRecorridos = input("Ingrese los kilometros que va a recorrer: ")
costoBoleto = (kilometrosRecorridos*PRECIOKILOMETRO)

print "Usted a recorrido ",kilometrosRecorridos," kilometros y el costo de su boleto es de: $",costoBoleto," pesos"
