#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Imprima la tabla de multitplicar de un numero N

resultado = 0
i = 1

tabla = int(input("¿Que tabla desea ver?: "))
numero = int(input("¿Hasta que numero desea ver?: "))

for i in range(numero):
    resultado = tabla * i
    print(tabla ," x ",i," = ", resultado)
    print("\n")
