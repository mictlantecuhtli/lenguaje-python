#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 10-08-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

calificacion = input ("Ingrese su calificacion: ")

if calificacion == 10:
    print "Sacaste A :D"
if calificacion == 9:
    print "Sacaste B :D"
if calificacion == 8:
    print "Sacaste C :)"
if calificacion == 7:
    print "Sacaste D :|"
if calificacion == 6:
    print "Sacaste D :|"
if calificacion == 5:
    print "Sacaste F :("
if calificacion == 4:
    print "Sacaste F :("
if calificacion == 3:
    print "Sacaste F :("
if calificacion == 2:
    print "Sacaste F :("
if calificacion == 1:
    print "Sacaste F"
