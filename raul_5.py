#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 04-01-2021                  #
#Autor: Fatima Azucena MC           #
#fatimaazucenamartinez274@gmail.com #
#####################################

#Calcular el nuevo salario de un empleado si a su
#salario actual se le incrementa 8% y se le 
#descuenta 0.25% por sus servicios

INCREMENTO = 0.8
DESCUENTO = 0.025
salario_actual = 0

nombre = input("Ingrese el nombre del empleado: ")
salario_actual =float(input("Ingrese su salario actual: "))

salario_actual = salario_actual + (salario_actual*INCREMENTO)
salario_actual = salario_actual - (salario_actual*DESCUENTO)

print ("El salario actual de ",nombre," es de: $",salario_actual," pesos")
