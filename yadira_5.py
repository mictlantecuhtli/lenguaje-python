#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#EN EL TECNOLÓGICO NACIONAL DE MEXICO SE PUBLICÓ UNA
#CONVOCATORIA DE ESTUDIOS EN EL EXTRANJERO, PODRÁN PARTICIPAR AQUELLOS
#ESTUDIANTES QUE TENGAN UN PROMEDIO MAYOR A 90. LOS ALUMNOS QUE HAYAN
#ALCANZADO LA CALIFICACIÓN ENTRARÁN DIRECTO A LA TÓMBOLA PARA EL SORTEO.
#LOS ALUMNOS QUE NO ALCANCEN EL PROMEDIO PODRÁN PARTICIPAR EN LA SIGUIENTE
#CONVOCATORIA SI APRUEBAN EL EXAMEN DEL IDIOMA PARA EL SIGUIENTE SEMESTRE
#Y MEJORAN EN UN 20% SU PROMEDIO

print("B I E N V E N I D O S ")
print("¿Vas a registrarte por primera vez?:\n1)Si\n2)No")
opcion = int(input("Digite su opcion: "))

if opcion == 1:
    print("ES TU PRIMERA VEZ PARTICIPANDO, SUERTE")
    calificacion = int(input("Digita tu calificacion (0-100): "))

    if calificacion > 90:
        print("FELICIDADES,HAZ ENTRADO DIRECTO A LA TOMBOLA")
    else:
        print("NO PUEDES ENTRAR A LA TOMBOLA, SUERTE PARA LA PROXIMA")

elif opcion == 2:
    print("TU YA HAZ PARTICIPADO ANTERIORMENTE, EL REQUISITO AHORA PARA QUE PUEDAS")
    print("ENTRAR ES: PASAR EL EXAMEN DE IDIOMAS Y HABER MEJORADO UN 20% EN TU")
    print("CALIFICACION")
    print("¿Pasaste el examen de idiomas?\n1)Si\n2)No")
    examen_Idiomas = int(input("Digite su opcion: "))

    if examen_Idiomas == 1:
        calificacion = int(input("Digite su calificacion actual (0-100): "))

        if calificacion > 90:
            print("FELICIDADES ENTRASTE A LA TOMBOLA, CUMPLES CON LOS DOS ")
            print("REQUSITOS")
        else:
            print("NO PUEDES ENTRAR A LA TOMBOLA, SUERTE PARA LA PRÓXIMA")
    
    elif examen_Idiomas == 2:
        print("NO PUEDES ENTRAR A LA TOMBOLA, YA QUE NO PASASTE UNO DE LOS ")
        print("REQUISITOS, SUERTE PARA LA PROXIMA")






