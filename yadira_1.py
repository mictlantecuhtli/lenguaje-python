#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Genere un programa que determine si eres mayor de edad

edad = int(input("Digite su edad: "))
if edad >= 18 :
    print ("Usted es mayor de edad :D")
else :
    print("Usted no es mayor de edad :(")
