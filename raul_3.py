#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

def switch():
    EUROS=22.9
    DOLLAR=21.9
    print("*Bienvenido al menú de conversiones*\n1)Dolares\n2)Euros ")
    opcion = input("Digite su opcion: ")

    if opcion == 1:
        print("Usted a elegido la conversion de pesos mexicanos a dolares")
        pesoMexicano = int(input("\nInserte cuanto dinero desea convertir: "))
        conversion = pesoMexicano/DOLLAR
        print("Usted tiene: $",conversion," dolares")

    elif opcion == 2:
        print("Usted a elegido la conversion de pesos mexicanos a euros: ")
        pesoMexicano = int(input("\nInserte cuanto dinero desea convertir: "))
        conversion = pesoMexicano/EURO

    else:
        print("Opcion no valida")
switch()
