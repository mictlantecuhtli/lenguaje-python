#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 24-11-2020                  #
#Autor: Fatima Azucena MC           #
#fatimaazucenamartinez274@gmail.com #
#####################################

#Calcular el nuevo salario de un empleado si se le descuenta el 20% de su salario actual.

DESCUENTO = 0.20
nombre = input ("Ingrese el nombre del empleado")
salario = input ("Ingrese su salario actual")

nuevo_Salario = nuevo_Salario-(salario*DESCUENTO)

print "El nuevo salario de ", nombre ," es de: $", nuevo_Salario ," pesos"
