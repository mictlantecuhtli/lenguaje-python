#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 19-08-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocidad constante.

kRecorridos = input ("Ingrese los kilometros recorridos: ")
kHora = input ("Ingrese los K/H: ")

tiempoLlegada = (kRecorridos/kHora)

print "El tiempo que tardara en llegar la persona es de: ", tiempoLlegada ," hrs"
