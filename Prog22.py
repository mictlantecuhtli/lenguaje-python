#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 10-08-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Se requiere determinar el costo que tendrá realizar una llamada te­lefónica con base en el tiempo que dura la llamada y en el costo por minuto. 

precioMinuto = input ("Ingrese el precio por minuto: ")
minuto = input ("Ingrese los minutos que tomo su llamada: ")

resultado = (precioMinuto*minuto)

print "El costo que tendra su llamada es de: $", resultado , "pesos"
