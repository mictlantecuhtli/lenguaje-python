#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys
import numpy

#####################################
#Fecha: 18-06-2021                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

def algoritmoBresenham(x1,y1,x2,y2):
    x = x1
    y = y1
    dx = x2 - x1
    dy = y2 - y1
    p = (2*dy) - dx
     
    while x <= x2:
        print("x: ", x, " y: ",y ,"\n")
        x += 1
        
        if p < 0:
            p = p + 2 * dy
        else:
            p = p + (2 * dy ) - (2 * dx)
            y += 1

a1 = int(input("Digite la coordenada de x1: "))
b1 = int(input("Digite la coordenada de y1: "))
a2 = int(input("Digite la coordenada de x2: "))
b2 = int(input("Digite la coordenada de y2: "))

function = algoritmoBresenham(a1,b1,a2,b2)
