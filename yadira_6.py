#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 11-01-2021                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#labora un programa con condicionales anidados que solicite 3 calificaciones, 
#obtén el promedio de esas tres calificaciones, y de acuerdo al promedio que 
#se obtuvo, coloca la leyenda que le corresponde, que encontraras en la imagen 
#que te comparto con el nombre NIVEL DE DESEMPEÑO-TESJI.JPG.  
#Por ejemplo si tu promedio se encuentra entre 95 y 100 deberá aparecer la 
#leyenda "EXCELENTE" //El documento se encuentra adjunto

calificacion_Fisica = int(input("Digite la calificacion que obtuvo en Física: "))
calificacion_Programacion = int(input("Digite la calificacion que obtuvo en Programacion: "))
calificacion_Historia = int(input("Digite la calificacion que obtuvo en Historia: "))

promedio_General = (calificacion_Fisica + calificacion_Historia + calificacion_Programacion)/3

if promedio_General >= 9.5 and promedio_Genereal <= 10.0 :
    print("EXCELENTE")
elif promedio_General >= 8.5 and promedio_General <= 9.4 :
    print("NOTABLE")
elif promedio_General >= 7.5 and promedio_General <= 8.0 :
    print("BUENO")
elif promedio_General >= 7.0 and promedio_General <= 7.4 :
    print("SUFICIENTE")
elif promedio_General < 7.0 :
     print("DESEMPEÑO INSUFICIENTE")
