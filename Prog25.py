#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 26-09-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#25-Realice un algoritmo que determine el sueldo semanal de N traba­jadores
#considerando que se les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos.
#Se les descuenta 7% si ganan más de 150 pero menos de 300, y 9% si ganan más de
#300 pero menos de 450. Los datos son horas trabajadas, sueldo por hora y nombre 
#de cada tra­bajador. Represéntelo mediante diagrama de flujo, pseudocódigo.
nombres = []
sueldoBruto = 0
sueldoNeto = 0
sueldoHoras = 0
canTrabajadores = 0

sueldoHoras=input("Inserte el precio por hora: ")
canTrabajadores=input("Inserte el numero de trabajadores: ")
for i in range (0, canTrabajadores, 1):
        print "Insete el nombre del trabajador: " 
        nombre=(input())
        nombres.append(nombre)
        hTrabajadas=int(input("Inserte las horas trabajadas: "))
        dineroGanado=float(input("Inserte el dinero ganado: "))
        if dineroGanado > 150 and dineroGanado <= 150 :
           sueldoBruto = 40 * (dineroGanado / hTrabajadas)
           sueldoNeto=sueldo - (sueldoNeto * 0.05)
        elif dineroGanado > 150 and dineroGanado < 350 :
           sueldoBruto=40*(dineroGanado/hTrabajadas)
           sueldoNeto=sueldoNeto-(sueldoNeto*0.07)
        elif dineroGanado >= 350 and dineroGanado <= 450 :
           sueldoBruto=40*(dineroGanado/hTrabajadas)
           sueldoNeto=sueldoNeto-(sueldoNeto*0.09)

print "El sueldo bruto de: ",nombre," es de: $",sueldoBruto," pesos"
print "El sueldo neto de: ",nombre," es de: $",sueldoNeto," pesos"

