#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 01-01-2021                  #
#Autor: Fatima Azucena MC           #
#fatimaazucenamartinez274@gmail.com #
#####################################

#Un vendedor recibe un sueldo base más un 10% por comision de sus ventas
#el vendedor desea saber cuanto dinero obtendra por concepto de comisiones
#por las 3 ventas que realiza en el mes y el total que recibira en el mes
#tomando en cunta su base y comisiones

comision = 0
sueldo_Total = 0 
total_Venta = 0

venta_1 =float(input("Ingrese el valor de la primer venta: "))
venta_2 =float(input("Ingrese el valor de la segunda venta: "))
venta_3 =float(input("Ingrese el valor de la tercer venta: "))
sueldo_Base = int(input("Ingrese su sueldo base: "))

total_Venta = venta_1 + venta_2 + venta_3
comision = comision + (total_Venta*0.10)
sueldo_Total = comision + sueldo_Base

print ("El sueldo total es de: $", sueldo_Total ," pesos")
print ("El total por concepto de comision es de: $", comision ," pesos")



