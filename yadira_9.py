#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Genere el factorial de un numero

numero_Factorial = 1
n = 1
numero = int(input("Digite un número: "))

for n in range (numero):
    numero_Factorial = numero_Factorial * n

print("El factorial de ", numero," es: ", numero_Factorial)

