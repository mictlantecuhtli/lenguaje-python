#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Tema Estructuras cíclicas
#1_EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER
#MAYOR INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O
#HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN
#HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
# • EDAD
# • LOCALIDAD
# • TIENEN INTERNET EN CASA
# • TIENEN COMPUTADORA EN CASA
# • CUENTA CON LOS RECURSOS BÁSICOS NECESARIOS DE ALIMENTACION.
#2_RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE
#DESCONOCE LA CANTIDAD DE ALUMNOS.
#3_IMPRIME LOS RESULTADOS CON NÚMEROS Y PORCENTAJES

localidad = []
edad = []
conInternet = 0
sinInternet = 0
conComputadora = 0
sinComputadora = 0
canalejas = 0
san_Juan = 0
com = 0
buena_Alimentacion = 0
mala_Alimentacion = 0

numero_Alumnos = int(input("Digite el numero de alumnos: "))

for i in range(0,numero_Alumnos, 1):
    print("Digite la edad del alumno ", i + 1 ,": ")
    val=int(input())
    edad.append(val)
    print("1)Cananelas\n2)San Juan Acazuchitlan\n3)Comunidad")
    print("Digite su opcion: ")
    comunidad=int(input())
    localidad.append(comunidad)
    
    if localidad == 1: 
        canalejas = canalejas + 1
    if localidad == 2:
        san_Juan = san_Juan + 1
    if localidad == 3:
        com = com + 1
    

    print("¿Cuenta con interner en casa?:\n1)Si\n0)No")
    internet = int(input("Digite su opcion: "))

    if internet == 1:
        conInternet = conInternet + 1
    elif internet == 0:
        sinInternet = sinInternet + 1

    print("¿Cuenta con computadora en casa?:\n1)Si\n0)No")
    computadora = int(input("Digite su opcion: "))

    if computadora == 1:
        conComputadora = conComputadora + 1
    elif computadora == 0:
        sinComputadora = sinComputadora + 1
    
    print("¿Cuenta con una buena alimentacion?:\n1)Si\n0)No")
    alimentacion = int(input("Digite su opcion: "))

    if alimentacion == 1:
        buena_Alimentacion = buena_Alimentacion + 1
    elif alimentacion == 0:
        mala_Alimentacion = mala_Alimentacion + 1

porcentaje = (100/numero_Alumnos)
internet_si = porcentaje*conInternet;
internet_no = porcentaje*sinInternet;
computadora_si = porcentaje*conComputadora;
computadora_no = porcentaje*sinComputadora;
alimentacion_mala = porcentaje*mala_Alimentacion;
alimentacion_buena = porcentaje*buena_Alimentacion;

print("La encuesta se aplico a ", numero_Alumnos," alumnos")
print(conInternet," alumnos dijeron que contaban con internet lo que")
print("corresponde a un ", internet_si," porciento")
print(sinInternet," alumnos dijeron que no contaban con internet lo que" )
print("corresponde a un ", internet_no," porciento")
print(conComputadora," alumnos dijeron que contaban con computadora lo que")
print("corresponde a un ", computadora_si," porciento")
print(sinComputadora," alumnos dijeron que no contaban con computadora lo que")
print("corresponde a un ", computadora_no)
print(buena_Alimentacion," dijeron que tenian una buena alimentacion lo que")
print("corresponde a un ", alimentacion_buena, " porciento")
print(mala_Alimentacion," dijeron que tenian una mala alimentacion lo que ")
print("corresponde a un ", alimentacion_mala," porciento")
 
