#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 05-01-2021                  #
#Autor: Fatima Azucena MC           #
#fatimaazucenamartinez274@gmail.com #
#####################################

#En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. 
#El presupuesto anual del hospital se reparte de la siguiente manera: 
#Pediatría 42% y Traumatología 21%.

presupuesto_Traumatologia = 0
presupuesto_Pediatria = 0
presupuesto_Urgencias = 0

presupuesto_Anual = float(input("Ingrese el presupuesto anual: "))

presupuesto_Traumatologia = presupuesto_Anual*0.21
presupuesto_Pediatria = presupuesto_Anual*0.42
presupuesto_Urgencias = presupuesto_Anual*0.37

print ("El presupuesto para el área de traumatología es de: $",presupuesto_Traumatologia," pesos")
print ("El presupuesto para el área de pediatria es de: $",presupuesto_Pediatria," pesos")
print ("El presupuesto para el área de urgencias es de: $",presupuesto_Urgencias," pesos")

