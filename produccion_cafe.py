#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys
import numpy

#####################################
#Fecha: 11-06-2021                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

nombres = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
suma_produccion = []
total_produccion = 0

print("Digite la produccion de cáfe en KG por meses: ")
for i in range(0,12):
    print("-->",nombres[i],": ")
    val=float(input())
    suma_produccion.append(val)
    total_produccion = total_produccion + suma_produccion[i]

total_produccion = total_produccion / 12;
print("La produccion anual es de: ", total_produccion);

print("Produccion mayor al promedio: ")
for i in range(0,12):
    if suma_produccion[i] > total_produccion:
        print("-->", suma_produccion[i])

print("Produccion menor al promedio: ")
for i in range(0,12):
    if suma_produccion[i] < total_produccion:
        print("-->", suma_produccion[i])

for x in range(0,12):
    for y in range(0,11):
        if suma_produccion[y] > suma_produccion[y+1]:
            posicion = suma_produccion[y+1]
            suma_produccion[y+1] = suma_produccion[y]
            suma_produccion[y] = posicion

print("La mayor produccion de cáfe es de: ", suma_produccion[11])
print("La menor produccion de cáfe es de: ", suma_produccion[0])
