#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys
import numpy

#####################################
#Fecha: 18-06-2021                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

def algoritmoDDA(x1, y1, x2, y2):
    dx = abs(x2-x1)
    dy = abs(y2-y1)

    if dx > dy:
        steps = dx
    else:
        steps = dy

    xinc = dx / steps
    yinc = dy / steps

    for i in range(0, steps, 1):
        print("{:.1f}".format ,"x: ", x1, " y: ",y1 ,"\n")
        x1 = x1 + xinc
        y1 = y1 + yinc

a1 = int(input("Digite la coordenada de x1: "))
b1 = int(input("Digite la coordenada de y1: "))
a2 = int(input("Digite la coordenada de x2: "))
b2 = int(input("Digite la coordenada de y2: "))

function = algoritmoDDA(a1,b1,a2,b2)



