#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 26-09-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#Realice un algoritmo para determinar qué cantidad de dinero hay en un monedero
#considerando que se tienen monedas de diez, cin­co y un peso, y billetes de diez
#veinte y cincuenta pesos. Represén­telo mediante diagrama de flujo, pseudocódigo.
resultado=0
billetes=[]
val=0
monedero = [1000, 500, 200, 100, 50, 20,10, 5, 2, 1, 0.50, 0.20, 0.10]
  
for i in range (len(monedero)) :
     print "Ingrese cuanto dinero de ",monedero[i]," tiene: "
     val=(input())
     billetes.append(val)
     resultado = resultado + (billetes[i] * monedero[i])

print "El total en el monedero es de: $",resultado," pesos"
