#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
#TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE FRECUENTE.
#LES OTORGARÁN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
total_Venta = 0
ventas_Descuento = []
ventas_no_Descuento = []

print("¿Cuenta con tarjeta de cliente frecuente?:\n\t1)Si\n\t2)No")
opcion = int(input("\tDigite su opcion: "))
if opcion == 1:
    numVentas = int(input("\n\tDigite el numero de ventas que realizo: "))

    for i in range(0, numVentas, 1):
        print("\n\tDigite el costo de la venta ", i + 1)
        val=float(input())
        ventas_Descuento.append(val)
        #almacena el total de ventas realizadas.
        total_Venta = total_Venta + ventas_Descuento[i]
        total_Venta = total_Venta - (total_Venta*0.20)
    
    print ("\n\tEl monto total es: $", total_Venta)

elif opcion == 2:
    numVentas = int(input("\n\tDigite el numero de ventas que realizo: "))
    for i in range(0, numVentas, 1):
        print("\n\tDigite el costo de la venta ", i + 1)
        val=float(input())
        ventas_no_Descuento.append(val)
        total_Venta = total_Venta + ventas_no_Descuento[i]


    print ("\n\tEl monto total es: $", total_Venta)


        

