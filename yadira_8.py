#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 02-10-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE

suma_Edades = 0
edad = []
numero_Alumnos=int(input("¿Cuantos alumnos son?: "))

for i in range(0, numero_Alumnos, 1):
        print("Digite la edad del alumno ", i + 1 ,": ")
        val=int(input())
        edad.append(val)
        suma_Edades = suma_Edades + edad[i]

suma_Edades = suma_Edades / numero_Alumnos
print("El promedio de las edades es de: ", suma_Edades ,"%")
