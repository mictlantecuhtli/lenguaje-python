#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 10-08-2020                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

#La compañía de autobuses “La curva loca” requiere determinar el costo que tendrá
#el boleto de un viaje sencillo, esto basado en los kilómetros por recorrer y en el 
#costo por kilómetro. Realice un diagrama de flujo y pseudocódigo que representen el 
#algoritmo para tal fin.

precioKilometro= input ("¿Cual es el costo del kilometro?: ")
kilometrosRecorridos = input ("¿Cuantos kilometros va a recorrer?: ")

costoBoleto=(precioKilometro*kilometrosRecorridos)

print "El precio del boleto es de: ", costoBoleto ," pesos"


